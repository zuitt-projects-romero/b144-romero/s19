
const exponent = 3;

const getCube = (x) => {
	return `The cube of ${x} is ${x ** exponent}`
}

console.log(getCube(3))


const address = [ "730", "Sta. Monica", "Lubao", "Pampanga"]

const [homeNumber, Brangay, City, Province] = address

console.log(`I live at ${homeNumber} ${Brangay} ${City} ${Province}`)

const animal = {
	name: "Red-tailed hawk",
	type: "large hawk",
	wingspan: "1-1.5m",
	color: "rich brown above and pale below",
	preys: "mammals",
	speed: "190 km/h"
};

const { name, type, wingspan, color, preys, speed } = animal

console.log( `${name} is a ${type}. with a wing span of ${wingspan} and color of ${color} and preys on ${preys} such as voles, rats, rabbits, and ground squirrels. With a speed of ${speed}`)

let number =[1, 2, 3, 4, 5]

number.forEach(enumeration)

function enumeration(num){ 
	return console.log(num)
}

const reduceNumber = (previousValue, currentValue) => previousValue + currentValue

console.log(number.reduce(reduceNumber));


class dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new dog();

myDog.name = 'Broly';
myDog.age = 2;
myDog.breed = 'American Bully';

console.log(myDog)